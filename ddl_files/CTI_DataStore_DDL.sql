
/*Data Store: CTI_DataStore
Created: 2018-07-10T15:30:41.940Z
By: cody.otto
Appian Version: 18.1.61.0
Target Database: MySQL 5.6.34-log
Database Driver: MySQL Connector Java mysql-connector-java-5.1.46 ( Revision: 9cc87a48e75c2d2e87c1a293b2862ce651cb256e )*/


    alter table `ctichangerequest_critems` 
        drop 
        foreign key FK708C3639384E0A1B;

    alter table `cticntract_otherteammembers` 
        drop 
        foreign key FKB626A2DADC4A81BB;

    alter table `cticontract_changerequests` 
        drop 
        foreign key FK2D63D2960C20B9D;

    alter table `cticontract_pos` 
        drop 
        foreign key FK988637BF1F941F43;

    alter table `ctidovico_taskassignments` 
        drop 
        foreign key FKA3CE93E48B38F3F3;

    alter table `ctiexpenserule_values` 
        drop 
        foreign key FKD7A0AC8572441167;

    drop table if exists `GLB_AuditInfo`;

    drop table if exists `ctiamendment`;

    drop table if exists `ctichangerequest_critems`;

    drop table if exists `ctichangerequest`;

    drop table if exists `ctichangerequestitem`;

    drop table if exists `cticntract_otherteammembers`;

    drop table if exists `cticontact`;

    drop table if exists `cticontract_changerequests`;

    drop table if exists `cticontract_pos`;

    drop table if exists `cticontract`;

    drop table if exists `cticustomer`;

    drop table if exists `ctidovico_taskassignments`;

    drop table if exists `ctidovico`;

    drop table if exists `ctiexpenseitem`;

    drop table if exists `ctiexpenserule_values`;

    drop table if exists `ctiexpenserule`;

    drop table if exists `ctifixedrateitem`;

    drop table if exists `ctihourlyrateitem`;

    drop table if exists `ctimilestone`;

    drop table if exists `ctipurchaseorder`;

    drop table if exists `ctitaskassignment`;

    drop table if exists `ctiteammemberrole`;

    create table `GLB_AuditInfo` (
        `CommentID` integer not null auto_increment,
        `ReferenceID` integer,
        `Event` varchar(200),
        `Action` varchar(200),
        `ActorLoginID` varchar(50),
        `ActorName` varchar(100),
        `AppName` varchar(100),
        `comment` varchar(1000),
        `CreatedOn` datetime,
        primary key (`CommentID`)
    ) ENGINE=InnoDB;

    create table `ctiamendment` (
        `recordid` integer not null auto_increment,
        `amendmentcontractid` integer,
        `amendmentdate` datetime,
        `amendmentnumber` integer,
        `originalcontractid` integer,
        primary key (`recordid`)
    ) ENGINE=InnoDB;

    create table `ctichangerequest_critems` (
        `ctichngrqst_critems_recrdid` integer not null,
        elt integer,
        `ctichangereqest_critems_idx` integer not null,
        primary key (`ctichngrqst_critems_recrdid`, `ctichangereqest_critems_idx`)
    ) ENGINE=InnoDB;

    create table `ctichangerequest` (
        `recordid` integer not null auto_increment,
        `approver` varchar(255),
        `contractid` integer,
        `customername` varchar(255),
        `date` datetime,
        `details` varchar(255),
        `impact` varchar(255),
        `projectname` varchar(255),
        `reason` varchar(255),
        `requestnum` varchar(255),
        `requestor` varchar(255),
        `senton` datetime,
        `signeddate` datetime,
        `totalamount` decimal(19,2),
        primary key (`recordid`)
    ) ENGINE=InnoDB;

    create table `ctichangerequestitem` (
        `recordid` integer not null auto_increment,
        `contractid` integer,
        `changerequestid` integer,
        `itemnum` integer,
        `description` varchar(255),
        `price` decimal(19,2),
        `usaffected` varchar(255),
        primary key (`recordid`)
    ) ENGINE=InnoDB;

    create table `cticntract_otherteammembers` (
        `cticntrct_thrtmmmbrs_rcrdid` integer not null,
        elt varchar(255),
        `cticntrct_thrtemmembers_idx` integer not null,
        primary key (`cticntrct_thrtmmmbrs_rcrdid`, `cticntrct_thrtemmembers_idx`)
    ) ENGINE=InnoDB;

    create table `cticontact` (
        `email` varchar(255) not null,
        `address` varchar(255),
        `fname` varchar(255),
        `lname` varchar(255),
        `phone` varchar(255),
        `position` varchar(255),
        primary key (`email`)
    ) ENGINE=InnoDB;

    create table `cticontract_changerequests` (
        `cticntrct_chngrqsts_recrdid` integer not null,
        elt integer,
        `cticntrct_changereqests_idx` integer not null,
        primary key (`cticntrct_chngrqsts_recrdid`, `cticntrct_changereqests_idx`)
    ) ENGINE=InnoDB;

    create table `cticontract_pos` (
        `cticontract_pos_recordid` integer not null,
        elt integer,
        `cticontract_pos_idx` integer not null,
        primary key (`cticontract_pos_recordid`, `cticontract_pos_idx`)
    ) ENGINE=InnoDB;

    create table `cticontract` (
        `recordid` integer not null auto_increment,
        `displayid` varchar(255),
        `customerid` integer,
        `status` varchar(255),
        `agreementtype` varchar(255),
        `bigprojectleader` varchar(255),
        `bigsalesperson` varchar(255),
        `billingrules` varchar(255),
        `contractamount` decimal(19,2),
        `customerprojectcontact` varchar(255),
        `dovico` integer,
        `invoicingtype` varchar(255),
        `linktobox` varchar(255),
        `plannedenddate` datetime,
        `platform` varchar(255),
        `projectlocation` varchar(255),
        `projectname` varchar(255),
        `projectstartdate` datetime,
        `salesforceopp` varchar(255),
        primary key (`recordid`)
    ) ENGINE=InnoDB;

    create table `cticustomer` (
        `recordid` integer not null auto_increment,
        `displayid` varchar(255),
        `name` varchar(255),
        `apcontact` varchar(255),
        `apmanager` varchar(255),
        `apnotes` varchar(255),
        `bigcompany` varchar(255),
        `billingaddress` varchar(255),
        `billingcurrency` varchar(255),
        `billingrules` varchar(255),
        `sfid` varchar(255),
        primary key (`recordid`)
    ) ENGINE=InnoDB;

    create table `ctidovico_taskassignments` (
        `ctidvic_tskssignmnts_rcrdid` integer not null,
        elt integer,
        `ctidvic_taskassignments_idx` integer not null,
        primary key (`ctidvic_tskssignmnts_rcrdid`, `ctidvic_taskassignments_idx`)
    ) ENGINE=InnoDB;

    create table `ctidovico` (
        `recordid` integer not null auto_increment,
        `projectleader` varchar(255),
        `startdate` datetime,
        primary key (`recordid`)
    ) ENGINE=InnoDB;

    create table `ctiexpenseitem` (
        `recordid` integer not null auto_increment,
        `contractid` integer,
        `customerid` integer,
        `comments` varchar(255),
        `expenseamount` decimal(19,2),
        primary key (`recordid`)
    ) ENGINE=InnoDB;

    create table `ctiexpenserule_values` (
        `ctiexpenserle_vales_recrdid` integer not null,
        elt varchar(255),
        `ctiexpenserule_values_idx` integer not null,
        primary key (`ctiexpenserle_vales_recrdid`, `ctiexpenserule_values_idx`)
    ) ENGINE=InnoDB;

    create table `ctiexpenserule` (
        `recordid` integer not null auto_increment,
        `contractid` integer,
        `type` varchar(255),
        `comments` varchar(255),
        primary key (`recordid`)
    ) ENGINE=InnoDB;

    create table `ctifixedrateitem` (
        `recordid` integer not null auto_increment,
        `contractid` integer,
        `customerid` integer,
        `contractamount` decimal(19,2),
        `date` datetime,
        `description` varchar(255),
        `milestoneid` integer,
        `name` varchar(255),
        `rateamount` decimal(19,2),
        `ratetype` varchar(255),
        `subtotal` decimal(19,2),
        primary key (`recordid`)
    ) ENGINE=InnoDB;

    create table `ctihourlyrateitem` (
        `recordid` integer not null auto_increment,
        `contractid` integer,
        `customerid` integer,
        `level` varchar(255),
        `rate` decimal(19,2),
        `teammember` varchar(255),
        `title` varchar(255),
        primary key (`recordid`)
    ) ENGINE=InnoDB;

    create table `ctimilestone` (
        `recordid` integer not null auto_increment,
        `actualdate` datetime,
        `contractid` integer,
        `customerid` integer,
        `description` varchar(255),
        `name` varchar(255),
        `planneddate` datetime,
        primary key (`recordid`)
    ) ENGINE=InnoDB;

    create table `ctipurchaseorder` (
        `po` varchar(255) not null,
        `linktobox` varchar(255),
        `maxamount` decimal(19,2),
        primary key (`po`)
    ) ENGINE=InnoDB;

    create table `ctitaskassignment` (
        `recordid` integer not null auto_increment,
        `assignee` varchar(255),
        `hours` decimal(19,2),
        `positionname` varchar(255),
        primary key (`recordid`)
    ) ENGINE=InnoDB;

    create table `ctiteammemberrole` (
        `recordid` integer not null auto_increment,
        `contractid` integer,
        `level` varchar(255),
        `teammember` varchar(255),
        `title` varchar(255),
        primary key (`recordid`)
    ) ENGINE=InnoDB;

    alter table `ctichangerequest_critems` 
        add index FK708C3639384E0A1B (`ctichngrqst_critems_recrdid`), 
        add constraint FK708C3639384E0A1B 
        foreign key (`ctichngrqst_critems_recrdid`) 
        references `ctichangerequest` (`recordid`);

    alter table `cticntract_otherteammembers` 
        add index FKB626A2DADC4A81BB (`cticntrct_thrtmmmbrs_rcrdid`), 
        add constraint FKB626A2DADC4A81BB 
        foreign key (`cticntrct_thrtmmmbrs_rcrdid`) 
        references `cticontract` (`recordid`);

    alter table `cticontract_changerequests` 
        add index FK2D63D2960C20B9D (`cticntrct_chngrqsts_recrdid`), 
        add constraint FK2D63D2960C20B9D 
        foreign key (`cticntrct_chngrqsts_recrdid`) 
        references `cticontract` (`recordid`);

    alter table `cticontract_pos` 
        add index FK988637BF1F941F43 (`cticontract_pos_recordid`), 
        add constraint FK988637BF1F941F43 
        foreign key (`cticontract_pos_recordid`) 
        references `cticontract` (`recordid`);

    alter table `ctidovico_taskassignments` 
        add index FKA3CE93E48B38F3F3 (`ctidvic_tskssignmnts_rcrdid`), 
        add constraint FKA3CE93E48B38F3F3 
        foreign key (`ctidvic_tskssignmnts_rcrdid`) 
        references `ctidovico` (`recordid`);

    alter table `ctiexpenserule_values` 
        add index FKD7A0AC8572441167 (`ctiexpenserle_vales_recrdid`), 
        add constraint FKD7A0AC8572441167 
        foreign key (`ctiexpenserle_vales_recrdid`) 
        references `ctiexpenserule` (`recordid`);

